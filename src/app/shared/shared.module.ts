import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormModule } from 'app/shared/dynamic-form/dynamic-form.module';
import { DynamicTableModule } from 'app/shared/dynamic-table/dynamic-table.module';

@NgModule({
  imports: [
    CommonModule,
    DynamicFormModule,
    DynamicTableModule
  ],
  declarations: []
})
export class SharedModule { }
