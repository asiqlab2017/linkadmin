import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'app/typescripts/free';
import { MDBBootstrapModulePro } from 'app/typescripts/pro';
import { UserDynamicFormComponent } from 'app/shared/dynamic-form/user-dynamic-form/user-dynamic-form.component';
import { DynamicFormService } from 'app/shared/dynamic-form/services/dynamic-form.service';
import { DynamicFormControlService } from 'app/shared/dynamic-form/services/dynamic-form-control.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MDBBootstrapModule.forRoot(),
    MDBBootstrapModulePro.forRoot(),
  ],
  bootstrap: [
    DynamicFormComponent
  ],
  exports: [
    DynamicFormComponent,
    UserDynamicFormComponent
  ],
  providers: [
    DynamicFormService,
    DynamicFormControlService
  ],
  declarations: [DynamicFormComponent, UserDynamicFormComponent]
})
export class DynamicFormModule { }
