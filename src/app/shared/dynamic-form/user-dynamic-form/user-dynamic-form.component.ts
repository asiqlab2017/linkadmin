import { Component } from '@angular/core';
import { DynamicFormComponent } from 'app/shared/dynamic-form/dynamic-form.component';
import { DynamicFormControlService } from 'app/shared/dynamic-form/services/dynamic-form-control.service';
import { DynamicFormService } from 'app/shared/dynamic-form/services/dynamic-form.service';

@Component({
  selector: 'link-admin-user-dynamic-form',
  templateUrl: './user-dynamic-form.component.html',
  styleUrls: ['./user-dynamic-form.component.scss']
})
export class UserDynamicFormComponent extends DynamicFormComponent {
  constructor(fs: DynamicFormControlService, dynamicFormService: DynamicFormService) {
    super(fs, dynamicFormService);
  }

  get isValid() {
      return this.form.controls[this.question.key].valid;
  }

}
