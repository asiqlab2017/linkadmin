import { Component, OnInit, Input } from '@angular/core';
import { DynamicFormBase } from 'app/shared/dynamic-form/models/dynamic-form-base';
import { FormGroup } from '@angular/forms';
import { DynamicFormControlService } from 'app/shared/dynamic-form/services/dynamic-form-control.service';
import { DynamicFormService } from 'app/shared/dynamic-form/services/dynamic-form.service';

@Component({
  selector: 'link-admin-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {

  @Input() questions: DynamicFormBase<any>[] = [];
  @Input() question: DynamicFormBase<any>;
  @Input() form: FormGroup;
  payLoad = '';

  constructor(private fs: DynamicFormControlService,
    private dynamicFormService: DynamicFormService) { }

    ngOnInit() {
      this.questions = this.dynamicFormService.getQuestions();
      this.form = this.fs.toFormGroup(this.questions);
    }

    onSubmit() {
      this.payLoad = JSON.stringify(this.form.value);
    }

}
