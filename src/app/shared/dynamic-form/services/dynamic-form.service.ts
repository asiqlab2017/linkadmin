import { Injectable } from '@angular/core';
import { DynamicFormBase } from '../models/dynamic-form-base';
import { DynamicFormTextbox } from '../models/dynamic-form-textbox';
import { DynamicFormDropdown } from 'app/shared/dynamic-form/models/dynamic-form-dropdown';

@Injectable()
export class DynamicFormService {

  // Todo: get from a remote source of question metadata
  // Todo: make asynchronous
  getQuestions() {

    const questions: DynamicFormBase<any>[] = [

      new DynamicFormDropdown({
        key: 'customer',
        label: 'Customers',
        options: [
          {value: 'solid',  label: 'Solid'},
          {value: 'great',  label: 'Great'},
          {value: 'good',   label: 'Good'},
          {value: 'unproven', label: 'Unproven'}
        ],
        order: 1
      }),

      new DynamicFormDropdown({
        key: 'role',
        label: 'Roles',
        options: [
          {value: 'solid',  label: 'Solid'},
          {value: 'great',  label: 'Great'},
          {value: 'good',   label: 'Good'},
          {value: 'unproven', label: 'Unproven'}
        ],
        order: 2
      }),

      new DynamicFormDropdown({
        key: 'country',
        label: 'Countries',
        options: [
          {value: 'solid',  label: 'Solid'},
          {value: 'great',  label: 'Great'},
          {value: 'good',   label: 'Good'},
          {value: 'unproven', label: 'Unproven'}
        ],
        order: 3
      }),

      new DynamicFormTextbox({
        key: 'firstName',
        label: 'First name',
        required: true,
        order: 4
      }),

      new DynamicFormTextbox({
        key: 'emailAddress',
        value: '',
        label: 'Email',
        type: 'email',
        order: 5
      }),

      new DynamicFormTextbox({
        key: 'companyName',
        value: '',
        label: 'Company Name',
        type: 'text',
        order: 6
      })
    ];

    return questions.sort((a, b) => a.order - b.order);
  }
}
