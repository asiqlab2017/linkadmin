import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ColumnMap, ColumnSetting } from 'app/shared/dynamic-table/models/dynamic-table-model';

@Component({
  selector: 'link-admin-dynamic-table',
  templateUrl: './dynamic-table.component.html',
  styleUrls: ['./dynamic-table.component.scss']
})
export class DynamicTableComponent implements OnInit, OnChanges {

  @Input() records: any[];
  @Input() settings: ColumnSetting[];
  keys: string[];
  columnMaps: ColumnMap[];
  isDesc = false;
  column = 'name';
  selectedRecords = {};
  constructor() { }

  ngOnChanges() {
    // this.keys = Object.keys(this.records[0]);
    console.log(this.settings);
    if (this.settings) {
      this.columnMaps = this.settings.map(col => new ColumnMap(col));
    } else {
      this.columnMaps = Object.keys(this.records[0])
        .map(key => {
          return new ColumnMap({ primaryKey: key });
        });
    }
  }

  ngOnInit() {
    this.sort(this.column);
  }

  sort(property) {
    this.isDesc = !this.isDesc;
    this.column = property;
    const direction = this.isDesc ? 1 : -1;

    this.records.sort(function(a, b) {
        if (a[property] < b[property]) {
            return -1 * direction;
        } else if ( a[property] > b[property]) {
            return 1 * direction;
        } else {
            return 0;
        }
    });
  };

}
