import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({    name: 'formatCell'})
export class FormatCellPipe implements PipeTransform {
    constructor (
        private currencyPipe: CurrencyPipe
    ) { }
    transform( value: any, format: string) {
        if ( value === undefined ) {
            return 'not available';
        } console.log('format', format);

        if (format === 'default') {
           // debugger;
            if (Array.isArray(value)) {
                if (typeof value[0] !== 'object') {
                    return value.join(' , ');

                } else {
                    return value.map(obj => {
                        return obj.name;
                    }).join( ' , ');
                }

            }
            if ( typeof value === 'object') {
                return value.name;
            }
        }
        if ( format === 'currency') {
            const value2 =  this.currencyPipe.transform(value, 'USD', 'symbol');
            const firstDigit = value2.match(/\d/);
            const symbol = value2.slice(0, firstDigit.index);
            const amount = value2.slice(firstDigit.index);
            return symbol + ' ' + amount;
        }
        return value;
    }
 }
