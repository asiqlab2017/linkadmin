import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { DynamicTableComponent } from './dynamic-table.component';
import { StyleCellDirective } from 'app/shared/dynamic-table/directives/style-cell-directive';
import { FormatCellPipe } from 'app/shared/dynamic-table/pipes/format-cell.pipe';
import { DynamicTableService } from 'app/shared/dynamic-table/services/dynamic-table-service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DynamicTableComponent, FormatCellPipe, StyleCellDirective],
  exports: [
    CommonModule,
    DynamicTableComponent
  ],
  providers: [CurrencyPipe, DynamicTableService]
})
export class DynamicTableModule { }
