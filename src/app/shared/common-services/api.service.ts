import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export interface ApiResponse {
    data?: any;
    status?: any;
}

@Injectable()
export class ApiService {
    baseUrl = 'http://devws.linknetwork.dk/api/en/';
    constructor(private httpClient: HttpClient) { }

    get(path: string): Observable<ApiResponse> {
        return this.httpClient.get(this.baseUrl + path).map(this._toAPIResponse);
    }

    _toAPIResponse(response: any) {
        return response;
    }
}
