import { Component, OnInit } from '@angular/core';
import { UsersService } from 'app/user/state/services/users.service';
import { ColumnSetting } from 'app/shared/dynamic-table/models/dynamic-table-model';

@Component({
  selector: 'link-admin-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  listofUsers: any = [];
  optionsSelectCustomers: Array<any>;
  optionsSelectRoles: Array<any>;
  optionsSelectCountries: Array<any>;
  availableCountries: any = [];
  availableCustomers: any = [];
  availableRoles: any = [];
  questions: any[];

  projectSettings: ColumnSetting [] = [
    {
      primaryKey : 'Name',
      header: 'Name'
    },
    {
      primaryKey: 'Email',
      header: 'Email'
    },
    {
      primaryKey: 'PhoneNumber',
      header: 'PhoneNumber'
    },
    {
      primaryKey: 'VAT',
      header: 'VAT NO'
    }
  ];

  constructor(private userService: UsersService) {
    this.onLoadFunctions();
  }

  ngOnInit() {
    console.log('WebShop Admin users Page Component');
    this.getUsersList();
    this.getAllCountries();
    this.getAllCustomers();
    this.getAllRoles();
  }

  onLoadFunctions() {
    this.userService.loadUsers();
    this.userService.loadCountries();
    this.userService.loadCustomers();
    this.userService.loadRoles();
    // this.questions = this.dynamicFormService.getQuestions();
    // console.log('this.questions :: ', this.questions);
  }

  getUsersList() {
    this.userService.getUsersList().subscribe(data => {
      if (data) {
        console.log(' User List data :: ', data);
        this.listofUsers = data;
        this.listofUsers = this.listofUsers.ItemsCollection;
        console.log('this.listofUsers :: ', this.listofUsers);
      }
    });
  }

  getAllCountries() {
    try {
      this.userService.getAllCountries().subscribe(
        data => {
          if (data) {
            console.log('data :: ', data);
            this.optionsSelectCountries = [];
            this.availableCountries = data;
            const defaultSelect = { 'value' : 'Select Country', 'label': 'Select Country' };
            this.optionsSelectCountries.push(defaultSelect);
            this.availableCountries.forEach(element => {
              const langVal = {
                'value': element.Alpha2Code,
                'label': element.Name,
              };
              this.optionsSelectCountries.push(langVal);
            });
            console.log('optionsSelectCountries :: ', this.optionsSelectCountries);
          }
        });
    } catch (error) {
      // this.toastService.error(error);
    }
  }

  getAllRoles() {
    try {
      this.userService.getAllRoles().subscribe(
        data => {
          if (data) {
            console.log('getAllRoles :: data :: ', data);
            this.optionsSelectRoles = [];
            this.availableRoles = data;
            const defaultSelect = { 'value' : 'Select Roles', 'label': 'Select Roles' };
            this.optionsSelectRoles.push(defaultSelect);
            this.availableRoles.forEach(element => {
              const countryVal = {
                'value': element.RoleID,
                'label': element.RoleName,
              };
              this.optionsSelectRoles.push(countryVal);
            });
            console.log('optionsSelectCountries :: ', this.optionsSelectRoles);
          }
        });
    } catch (error) {
      // this.toastService.error(error);
    }
  }

  getAllCustomers() {
    try {
      this.userService.getAllCustomers().subscribe(
        data => {
          if (data) {
            console.log('getAllCustomers :: data :: ', data);
            this.optionsSelectCustomers = [];
            this.availableCustomers = data;
            const defaultSelect = { 'value' : 'Select Customers', 'label': 'Select Customers' };
            this.optionsSelectCustomers.push(defaultSelect);
            this.availableCustomers.forEach(element => {
              const customerVal = {
                'value': element.CustomerID,
                'label': element.Name,
              };
              this.optionsSelectCustomers.push(customerVal);
            });
            console.log('optionsSelectCustomers :: ', this.optionsSelectCustomers);
          }
        });
    } catch (error) {
      // this.toastService.error(error);
    }
  }

}
