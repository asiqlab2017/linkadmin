import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './user.component';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { UsersEffects } from 'app/user/state/effects';
import { usersReducers } from 'app/user/state/reducers/users.reducer';
import { MDBBootstrapModule } from 'app/typescripts/free';
import { MDBBootstrapModulePro } from 'app/typescripts/pro';
import { SharedModule } from 'app/shared/shared.module';
import { DynamicFormModule } from 'app/shared/dynamic-form/dynamic-form.module';
import { UsersHttpService } from 'app/user/state/services/users.http.service';
import { UsersService } from 'app/user/state/services/users.service';
import { DynamicTableModule } from 'app/shared/dynamic-table/dynamic-table.module';

@NgModule({
  imports: [
    CommonModule,
    MDBBootstrapModule.forRoot(),
    MDBBootstrapModulePro.forRoot(),
    SharedModule,
    DynamicFormModule,
    DynamicTableModule,
    EffectsModule.forFeature([UsersEffects]),
    StoreModule.forFeature('usersModule', usersReducers)
  ],
  declarations: [UserComponent],
  providers: [UsersHttpService, UsersService]
})
export class UserModule { }
