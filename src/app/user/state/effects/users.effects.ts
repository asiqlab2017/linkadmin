import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store, Action } from '@ngrx/store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import { Effect, Actions } from '@ngrx/effects';
import { UserActions } from '../actions/index';
import { State } from '../reducers/index';
import { UsersHttpService } from '../services/users.http.service';

@Injectable()
export class UsersEffects {

    @Effect()
    getUsersList$: Observable<Action> = this.actions$
        .ofType(UserActions.LOAD_USERS)
        .withLatestFrom(this.store$)
        .switchMap(([]: any) => {
            return this.usersHttpService.getUsersList().map((data) => {
                return new UserActions.LoadUsersSuccess(data);
            }).catch((error: any) => {
                return Observable.of(new UserActions.LoadUsersFail(error));
            });
        });

    @Effect()
    getAllCountries$: Observable<Action> = this.actions$
        .ofType(UserActions.LOAD_COUNTRIES)
        .withLatestFrom(this.store$)
        .switchMap(([]: any) => {
            return this.usersHttpService.getAllCountries().map((data) => {
                console.log('getAllCountries effects data :: ', data);
                return new UserActions.LoadCountriesSuccess(data);
            }).catch((error: any) => {
                return Observable.of(new UserActions.LoadCountriesFail(error));
            });
        });

    @Effect()
    getAllCustomers$: Observable<Action> = this.actions$
        .ofType(UserActions.LOAD_CUSTOMERS)
        .withLatestFrom(this.store$)
        .switchMap(([]: any) => {
            return this.usersHttpService.getAllCustomers().map((data) => {
                console.log('getAllCustomers effects data :: ', data);
                return new UserActions.LoadCustomersSuccess(data);
            }).catch((error: any) => {
                return Observable.of(new UserActions.LoadCountriesFail(error));
            });
        });

    @Effect()
    getAllRoles$: Observable<Action> = this.actions$
        .ofType(UserActions.LOAD_ROLES)
        .withLatestFrom(this.store$)
        .switchMap(([]: any) => {
            return this.usersHttpService.getAllRoles().map((data) => {
                console.log('getAllRoles effects data :: ', data);
                return new UserActions.LoadRolesSuccess(data);
            }).catch((error: any) => {
                return Observable.of(new UserActions.LoadCountriesFail(error));
            });
        });

    constructor(private actions$: Actions,
        private store$: Store<State>,
        private usersHttpService: UsersHttpService) { }
}
