import { Injectable } from '@angular/core';
import { ApiService } from 'app/shared/common-services/api.service';

@Injectable()
export class UsersHttpService {
    constructor(private apiService: ApiService) {}

    getUsersList() {
        const path = 'users/list/1?pageIndex=1&pageSize=10&roleID=&name=&email=&companyName=&country=&roleCode=Webshop_Admin';
        return this.apiService.get(path).map((res) => {
            console.log('users list res :: ', res);
            return res;
        });
    }

    getAllCountries() {
        const path = 'countries';
        return this.apiService.get(path).map((res) => {
            console.log('countries list res :: ', res);
            return res;
        });
    }

    getAllRoles() {
        const path = 'roles?type=webshop';
        return this.apiService.get(path).map((res) => {
            console.log('roles list res :: ', res);
            return res;
        });
    }

    getAllCustomers() {
        const path = 'customers/list?ParentCustomerID=1';
        return this.apiService.get(path).map((res) => {
            console.log('customers list res :: ', res);
            return res;
        });
    }

}
