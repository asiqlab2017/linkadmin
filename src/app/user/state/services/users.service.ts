import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersState } from '../reducers/users.reducer';
import { UserActions } from '../actions/index';
import { getUserListSuccess, getAllCountries, getAllCustomers, getAllRoles } from '../reducers/index';

@Injectable()
export class UsersService {
    constructor(private store: Store<UsersState>) {}

loadUsers() {
    this.store.dispatch(new UserActions.LoadUsers());
}

loadCountries() {
    this.store.dispatch(new UserActions.LoadCountries());
}

loadCustomers() {
    this.store.dispatch(new UserActions.LoadCustomers());
}

loadRoles() {
    this.store.dispatch(new UserActions.LoadRoles());
}

getUsersList() {
    return this.store.select(getUserListSuccess);
}

getAllCountries() {
    return this.store.select(getAllCountries);
}

getAllCustomers() {
    return this.store.select(getAllCustomers);
}

getAllRoles() {
    return this.store.select(getAllRoles);
}

}
