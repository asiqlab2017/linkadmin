import { Action } from '@ngrx/store';
export const LOAD_USERS = '[Users] Load all users';
export const LOAD_USERS_SUCCESS = '[Users] Load Users Success';
export const LOAD_USERS_FAIL = '[Users] Load Users Fail';
export const LOAD_COUNTRIES = '[Users] Load all countries';
export const LOAD_COUNTRIES_SUCCESS = '[Users] Load Countries Success';
export const LOAD_COUNTRIES_FAIL = '[Users] Load Countries Fail';

export const LOAD_CUSTOMERS = '[Users] Load All Customers';
export const LOAD_CUSTOMERS_SUCCESS = '[Users] Load Customers Success';
export const LOAD_CUSTOMERS_FAIL = '[Users] Load Customers Fail';

export const LOAD_ROLES = '[Users] Load All Roles';
export const LOAD_ROLES_SUCCESS = '[Users] Load Roles Success';
export const LOAD_ROLES_FAIL = '[Users] Load Roles Fail';

export class LoadUsers implements Action {
   readonly type = LOAD_USERS;
}

export class LoadUsersSuccess implements Action {
    readonly type = LOAD_USERS_SUCCESS;
    constructor(public payload: any) {}
}

export class LoadUsersFail implements Action {
    readonly type = LOAD_USERS_FAIL;
    constructor(public payload: any) {}
}

export class LoadCountries implements Action {
    readonly type = LOAD_COUNTRIES;
    constructor() {}
}

export class LoadCountriesSuccess implements Action {
    readonly type = LOAD_COUNTRIES_SUCCESS;
    constructor(public payload: any) {}
}

export class LoadCountriesFail implements Action {
    readonly type = LOAD_COUNTRIES_FAIL;
    constructor(public payload: any) {}
}

export class LoadCustomers implements Action {
    readonly type = LOAD_CUSTOMERS;
    constructor() {}
}

export class LoadCustomersSuccess implements Action {
    readonly type = LOAD_CUSTOMERS_SUCCESS;
    constructor(public payload: any) {}
}

export class LoadCustomersFail implements Action {
    readonly type = LOAD_CUSTOMERS_FAIL;
    constructor(public payload: any) {}
}

export class LoadRoles implements Action {
    readonly type = LOAD_ROLES;
    constructor() {}
}

export class LoadRolesSuccess implements Action {
    readonly type = LOAD_ROLES_SUCCESS;
    constructor(public payload: any) {}
}

export class LoadRolesFail implements Action {
    readonly type = LOAD_ROLES_FAIL;
    constructor(public payload: any) {}
}


export type All =
    | LoadUsers
    | LoadUsersSuccess
    | LoadUsersFail
    | LoadCountries
    | LoadCountriesSuccess
    | LoadCountriesFail
    | LoadCustomers
    | LoadCustomersSuccess
    | LoadCustomersFail
    | LoadRoles
    | LoadRolesSuccess
    | LoadRolesFail
