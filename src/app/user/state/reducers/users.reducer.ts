import { Users } from '../models/index';
import { UserActions } from '../actions/index';


export interface UsersState {
    users: Users[];
    countries: any;
    customers: any;
    roles: any;
}

export const initialState: UsersState = {
    users: null,
    countries: null,
    customers: null,
    roles: null,
};

export function usersReducers(state: UsersState = initialState, action: UserActions.All) {
    switch (action.type) {

        case UserActions.LOAD_USERS_SUCCESS:
            return Object.assign({}, state, { users : action.payload} );

        case UserActions.LOAD_COUNTRIES_SUCCESS:
            console.log('countries :: ', action.payload);
            return Object.assign({}, state, { countries: action.payload} );

        case UserActions.LOAD_CUSTOMERS_SUCCESS:
            console.log('Customers :: ', action.payload);
            return Object.assign({}, state, { customers: action.payload} );

        case UserActions.LOAD_ROLES_SUCCESS:
            console.log('Roles :: ', action.payload);
            return Object.assign({}, state, { roles: action.payload} );

        default: {
            return state;
        }
    }
}
