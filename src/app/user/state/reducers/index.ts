import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UsersState, usersReducers } from './users.reducer';

export interface State {
    usersModule: UsersState;
}

export const reducers = usersReducers;

export const getUsersState = createFeatureSelector<UsersState>('usersModule');

export const getUserListSuccess = createSelector(getUsersState,
    (state: UsersState) => state.users);

export const getAllCountries = createSelector(getUsersState,
    (state: UsersState) => state.countries);

export const getAllCustomers = createSelector(getUsersState,
    (state: UsersState) => state.customers);

export const getAllRoles = createSelector(getUsersState,
    (state: UsersState) => state.roles);
