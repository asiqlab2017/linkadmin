
export interface PersonalDetails {
    name: string;
    userName: string;
    department: string;
    jobPosition: string;
    role: any;
    language: any;
}

export interface Address {
    companyName: string;
    phoneNumber: number;
    email: string;
    address1: string;
    address2: string;
    address3: string;
    city: string;
    postalCode: string;
    country: any;
}

export interface UserInfo {
    vatNumber: number;
    comment: string;
    paymentByInvoice: boolean;
    uploadImage: any;
    previousImage: any;
}

export interface Users {
    personalDetails: PersonalDetails;
    address: Address;
    userInfo: UserInfo;
}


