import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'app/home/home.component';
import { UserComponent } from 'app/user/user.component';

const adminRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/admin-home' },
  { path: 'admin-home', component: HomeComponent },
  { path: 'admin-user', component: UserComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRouterModule { }
